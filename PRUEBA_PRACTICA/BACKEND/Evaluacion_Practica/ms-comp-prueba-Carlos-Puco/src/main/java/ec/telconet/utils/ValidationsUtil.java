package ec.telconet.utils;

import java.util.regex.Pattern;

public class ValidationsUtil {

    public static void validateEmail(String email) throws ValidationException {
        String emailRegex = "^[A-Za-z0-9+_.-]+@[A-Za-z0-9.-]+$";
        if (!Pattern.matches(emailRegex, email)) {
            throw new ValidationException("El campo usuario debe ser un correo electrónico válido.");
        }
    }

    public static void validatePassword(String password) throws ValidationException {
        if (password.length() > 8 || !Pattern.compile("^(?=.*[A-Z])(?=.*[@#$%^&+=]).*$").matcher(password).matches()) {
            throw new ValidationException("El campo password debe tener máximo 8 caracteres y contener al menos una mayúscula y un carácter especial (@#$%^&+=).");
        }
    }

    public static void validateUppercase(String value, String fieldName) throws ValidationException {
        if (!value.matches("[A-Z]+")) {
            throw new ValidationException("El campo " + fieldName + " debe contener solo mayúsculas.");
        }
    }

    public static void validateLowercase(String value, String fieldName) throws ValidationException {
        if (!value.matches("[a-z\\s]+")) {
            throw new ValidationException("El campo " + fieldName + " debe contener solo minúsculas y espacios.");
        }
    }
}