package ec.telconet.service;

import java.util.List;
import ec.telconet.entity.AdmiRol;

public interface AdminRolService {
    List<AdmiRol> getAllAdminRoles();

    AdmiRol getAdminRolById(Long id);

    AdmiRol createAdminRol(AdmiRol adminRol);

    AdmiRol updateAdminRol(Long id, AdmiRol adminRol);
    AdmiRol changeRoleState(Long id);

    boolean deleteAdminRol(Long id);

}