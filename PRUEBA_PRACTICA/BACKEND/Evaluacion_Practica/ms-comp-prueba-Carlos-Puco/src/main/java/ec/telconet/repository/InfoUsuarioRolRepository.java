package ec.telconet.repository;


import java.util.List;
import ec.telconet.entity.InfoUsuarioRol;
import org.springframework.data.jpa.repository.JpaRepository;



public interface InfoUsuarioRolRepository extends JpaRepository<InfoUsuarioRol, Long> {
    InfoUsuarioRol findByUsuarioIdAndRolId(Long usuarioId, Long rolId);
    List<InfoUsuarioRol> findByRolId(Long rolId);
    List<InfoUsuarioRol> findAllByUsuarioId(Long usuarioId);
    List<InfoUsuarioRol> findByUsuarioId(Long usuarioId);
}
