package ec.telconet.service.implement;

import java.util.Date;
import ec.telconet.entity.AdmiRol;
import ec.telconet.entity.AdmiUsuario;
import ec.telconet.entity.InfoUsuarioRol;
import ec.telconet.repository.AdmiRolRepository;
import ec.telconet.repository.AdmiUsuarioRepository;
import ec.telconet.repository.InfoUsuarioRolRepository;
import ec.telconet.service.InicializacionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class InicializacionServiceImpl implements InicializacionService {
    @Autowired
    private  AdmiRolRepository rolRepository;
    @Autowired
    private  AdmiUsuarioRepository usuarioRepository;
    @Autowired
    private  InfoUsuarioRolRepository infoUsuarioRolRepository;

    @Override
    @Transactional
    public void inicializar() {
        // Crear el rol ADMINISTRADOR si no existe
        AdmiRol administradorRol = rolRepository.findByNombre("ADMINISTRADOR");
        if (administradorRol == null) {
            administradorRol = new AdmiRol();
            administradorRol.setNombre("ADMINISTRADOR");
            administradorRol.setStatus("Activo");
            administradorRol.setDescripcion("Rol de administrador");
            rolRepository.save(administradorRol);
        }

        // Crear el usuario administrador si no existe
        AdmiUsuario administradorUsuario = usuarioRepository.findByUsuario("administrador@prueba.com");
        if (administradorUsuario == null) {
            administradorUsuario = new AdmiUsuario();
            administradorUsuario.setUsuario("administrador@prueba.com");
            administradorUsuario.setPassword("password"); // Asigna la contraseña deseada
            administradorUsuario.setNombres("Administrador");
            administradorUsuario.setApellidos("Prueba");
            administradorUsuario.setDireccion("Dirección de Administrador");
            administradorUsuario.setTelefono(1234567890L);
            administradorUsuario.setStatus("Activo");
            administradorUsuario.setUsuarioCreacion("admin");
            administradorUsuario.setFechaCreacion(new Date());
            administradorUsuario.setFoto("dsadsawqdsad.jpg");
            usuarioRepository.save(administradorUsuario);
        }

        // Asociar el rol ADMINISTRADOR al usuario administrador con estado Activo si no está asociado
        InfoUsuarioRol infoUsuarioRol = infoUsuarioRolRepository.findByUsuarioIdAndRolId(administradorUsuario.getId(), administradorRol.getId());
        if (infoUsuarioRol == null) {
            infoUsuarioRol = new InfoUsuarioRol();
            infoUsuarioRol.setUsuario(administradorUsuario);
            infoUsuarioRol.setRol(administradorRol);
            infoUsuarioRol.setUsuarioCreacion("admin");
            infoUsuarioRol.setFechaCreacion(new Date());
            infoUsuarioRol.setEstado("Activo");
            infoUsuarioRolRepository.save(infoUsuarioRol);
        }
    }
}
