package ec.telconet.service;

import java.util.List;
import ec.telconet.entity.AdmiRol;
import ec.telconet.entity.AdmiUsuario;
import ec.telconet.models.AdmiUsuarioRequest;
import jakarta.persistence.Tuple;

public interface AdmiUsuarioService {
    List<AdmiUsuario> getAllAdmiUsuarios();
    AdmiUsuario getAdmiUsuarioById(Long id);
    AdmiUsuario createAdmiUsuario(AdmiUsuarioRequest admiUsuario);
    AdmiUsuario updateAdmiUsuario(Long id, AdmiUsuario admiUsuario);
    boolean deleteAdmiUsuario(Long id);
    AdmiUsuario getAdmiUsuarioByUsername(String username);
    AdmiUsuario changeUserState(Long id);
    List<Tuple> getUserRoles(Long userId);
    List<AdmiRol> getAllRolesByUserId(Long userId);

}
