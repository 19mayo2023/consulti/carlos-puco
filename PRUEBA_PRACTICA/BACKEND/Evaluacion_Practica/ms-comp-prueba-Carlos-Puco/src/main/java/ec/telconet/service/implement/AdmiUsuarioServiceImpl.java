package ec.telconet.service.implement;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import ec.telconet.entity.AdmiRol;
import ec.telconet.entity.AdmiUsuario;
import ec.telconet.entity.InfoUsuarioRol;
import ec.telconet.models.AdmiUsuarioRequest;
import ec.telconet.repository.AdmiUsuarioRepository;
import ec.telconet.repository.InfoUsuarioRolRepository;
import ec.telconet.service.AdmiUsuarioService;
import ec.telconet.utils.ValidationException;
import ec.telconet.utils.ValidationsUtil;
import jakarta.persistence.Tuple;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.stereotype.Service;

@Service
public class AdmiUsuarioServiceImpl implements AdmiUsuarioService {

    private final AdmiUsuarioRepository admiUsuarioRepository;

    private final InfoUsuarioRolRepository infoUsuarioRolRepository;
    @Autowired
    public AdmiUsuarioServiceImpl(AdmiUsuarioRepository admiUsuarioRepository, InfoUsuarioRolRepository infoUsuarioRolRepository) {
        this.admiUsuarioRepository = admiUsuarioRepository;
        this.infoUsuarioRolRepository = infoUsuarioRolRepository;
    }

           /*
     * funtion: Funcion para que al momento de borara el rol el usuario ya no tenga la asiciacion
     * return: List<AdmiUsuario>
     * param:any
     * author: carlos.puco.mb@gamil.com
     * version:1.0
     */

    @Override
    public List<AdmiUsuario> getAllAdmiUsuarios() {
        return admiUsuarioRepository.findAll();
    }

              /*
     * funtion: Funcion para traer el rol administrador
     * retunr: AdmiUsuario
     * param:Long id
     * author: carlos.puco.mb@gamil.com
     * version:1.0
     */

    @Override
    public AdmiUsuario getAdmiUsuarioById(Long id) {
        return admiUsuarioRepository.findById(id).orElse(null);
    }

              /*
     * funtion: Funcion para crear un nuevo usuario
     * return: AdmiUsuario
     * param:AdmiUsuarioRequest admiUsuario
     * author: carlos.puco.mb@gamil.com
     * version:1.0
     */

    @Override
    public AdmiUsuario createAdmiUsuario(AdmiUsuarioRequest admiUsuario) throws ValidationException {
        admiUsuario.getAdmiUsuario().setStatus("Activo");
        String usuario = admiUsuario.getAdmiUsuario().getUsuario();
        String password = admiUsuario.getAdmiUsuario().getPassword();
        String nombres = admiUsuario.getAdmiUsuario().getNombres();
        String apellidos = admiUsuario.getAdmiUsuario().getApellidos();
        String direccion = admiUsuario.getAdmiUsuario().getDireccion();
        AdmiUsuario admiUser=new AdmiUsuario();
        try {
            ValidationsUtil.validateEmail(usuario);
            ValidationsUtil.validatePassword(password);
            ValidationsUtil.validateUppercase(nombres, "nombres");
            ValidationsUtil.validateUppercase(apellidos, "apellidos");
            ValidationsUtil.validateLowercase(direccion, "direccion");

            admiUsuario.getAdmiUsuario().setFechaCreacion(new Date());
            admiUser =admiUsuarioRepository.save(admiUsuario.getAdmiUsuario());
            for (AdmiRol admiRol : admiUsuario.getAdmiRolList()) {
                InfoUsuarioRol infoUsuarioRol = new InfoUsuarioRol();
                infoUsuarioRol.setUsuario(admiUsuario.getAdmiUsuario());
                infoUsuarioRol.setRol(admiRol);
                infoUsuarioRol.setUsuarioCreacion(admiUsuario.getAdmiUsuario().getUsuarioCreacion());
                infoUsuarioRol.setFechaCreacion(new Date());
                infoUsuarioRol.setEstado("Activo");
                infoUsuarioRolRepository.save(infoUsuarioRol);
            }


        } catch (ValidationException e) {
            throw  e;
        }
        return admiUser;
    }

              /*
     * funtion: Funcion para editar un usuario
     * retunr: AdmiUsuario
     * param:Long id, AdmiUsuario admiUsuario
     * author: carlos.puco.mb@gamil.com
     * version:1.0
     */

    @Override
    public AdmiUsuario updateAdmiUsuario(Long id, AdmiUsuario admiUsuario) throws ValidationException {
        String usuario = admiUsuario.getUsuario();
        String password = admiUsuario.getPassword();
        String nombres = admiUsuario.getNombres();
        String apellidos = admiUsuario.getApellidos();
        String direccion = admiUsuario.getDireccion();
        try {
            ValidationsUtil.validateEmail(usuario);
            ValidationsUtil.validatePassword(password);
            ValidationsUtil.validateUppercase(nombres, "nombres");
            ValidationsUtil.validateUppercase(apellidos, "apellidos");
            ValidationsUtil.validateLowercase(direccion, "direccion");
            AdmiUsuario existingAdmiUsuario = admiUsuarioRepository.findById(id).orElse(null);
            if (existingAdmiUsuario != null) {
                existingAdmiUsuario.setUsuario(admiUsuario.getUsuario());
                existingAdmiUsuario.setPassword(admiUsuario.getPassword());
                existingAdmiUsuario.setNombres(admiUsuario.getNombres());
                existingAdmiUsuario.setApellidos(admiUsuario.getApellidos());
                existingAdmiUsuario.setDireccion(admiUsuario.getDireccion());
                existingAdmiUsuario.setTelefono(admiUsuario.getTelefono());
                existingAdmiUsuario.setUsuarioCreacion(admiUsuario.getUsuarioCreacion());
                existingAdmiUsuario.setFechaCreacion(admiUsuario.getFechaCreacion());
                existingAdmiUsuario.setFoto(admiUsuario.getFoto());
                return admiUsuarioRepository.save(existingAdmiUsuario);
            }
        }catch(ValidationException e){
                throw  e;
            }
        return null;
    }

              /*
     * funtion: Funcion para eliminar un usuario
     * param: Long id
     * return: boolean
     * author: carlos.puco.mb@gamil.com
     * version:1.0
     */

    @Override
    public boolean deleteAdmiUsuario(Long id) {
        Optional<AdmiUsuario> optionalAdmiUsuario = admiUsuarioRepository.findById(id);
        if (optionalAdmiUsuario.isPresent()) {
            AdmiUsuario admiUsuario = optionalAdmiUsuario.get();

            // Desasociar el rol del usuario
            List<InfoUsuarioRol> infoUsuarioRoles = infoUsuarioRolRepository.findByUsuarioId(admiUsuario.getId());
            for (InfoUsuarioRol infoUsuarioRol : infoUsuarioRoles) {
                infoUsuarioRolRepository.delete(infoUsuarioRol);
            }

            admiUsuarioRepository.delete(admiUsuario);
            return true;
        }
        return false;
    }

              /*
     * funtion: Funcion para traer el usuario administrador
     * param: String username
     * return: AdmiUsuario
     * author: carlos.puco.mb@gamil.com
     * version:1.0
     */

    @Override
    public AdmiUsuario getAdmiUsuarioByUsername(String username) {
        return admiUsuarioRepository.findByUsuario(username);
    }

              /*
     * funtion: Funcion para cambiar el esatdo del usuario
     * param: List<AdmiUsuario>
     * return:any
     * author: carlos.puco.mb@gamil.com
     * version:1.0
     */
    @Override
    public AdmiUsuario changeUserState(Long id) {
        AdmiUsuario admiUsuario = admiUsuarioRepository.findById(id).orElse(null);
        if (admiUsuario != null) {
            admiUsuario.setStatus(admiUsuario.getStatus().equals("Activo") ? "Inactivo" : "Activo");
            return admiUsuarioRepository.save(admiUsuario);
        }
        return null;
    }

          /*
     * funtion: Funcion para traer los nuevos roles
     * param: List<AdmiUsuario>
     * return:any
     * author: carlos.puco.mb@gamil.com
     * version:1.0
     */

    @Override
    public List<Tuple> getUserRoles(Long userId) {
        List<Tuple> admiRol = admiUsuarioRepository.findAllById(userId);
        return admiRol;
    }
              /*
     * funtion: Funcion para   todos los roles 
     * param: List<AdmiUsuario>
     * return:any
     * author: carlos.puco.mb@gamil.com
     * version:1.0
     */
    @Override
    public List<AdmiRol> getAllRolesByUserId(Long userId) {
        List<InfoUsuarioRol> infoUsuarioRoles = infoUsuarioRolRepository.findAllByUsuarioId(userId);
        List<AdmiRol> roles = new ArrayList<>();
        for (InfoUsuarioRol infoUsuarioRol : infoUsuarioRoles) {
            roles.add(infoUsuarioRol.getRol());
        }
        return roles;
    }
}