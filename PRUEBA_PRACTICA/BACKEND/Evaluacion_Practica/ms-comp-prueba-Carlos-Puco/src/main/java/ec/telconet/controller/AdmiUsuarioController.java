package ec.telconet.controller;

import java.util.List;
import ec.telconet.entity.AdmiRol;
import ec.telconet.entity.AdmiUsuario;
import ec.telconet.models.AdmiUsuarioRequest;
import ec.telconet.service.AdmiUsuarioService;
import ec.telconet.utils.ErrorResponse;
import ec.telconet.utils.ValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/admi-usuarios")
public class AdmiUsuarioController {
    private final AdmiUsuarioService admiUsuarioService;


    @Autowired
    public AdmiUsuarioController(AdmiUsuarioService admiUsuarioService) {
        this.admiUsuarioService = admiUsuarioService;
    }

    @GetMapping("/all")
    public ResponseEntity<List<AdmiUsuario>> getAllAdmiUsuarios() {
        List<AdmiUsuario> admiUsuarios = admiUsuarioService.getAllAdmiUsuarios();
        return ResponseEntity.ok(admiUsuarios);
    }

    @GetMapping("userById/{id}")
    public ResponseEntity<AdmiUsuario> getAdmiUsuarioById(@PathVariable Long id) {
        AdmiUsuario admiUsuario = admiUsuarioService.getAdmiUsuarioById(id);
        if (admiUsuario != null) {
            return ResponseEntity.ok(admiUsuario);
        } else {
            return ResponseEntity.notFound().build();
        }
    }
    @GetMapping("/by-username/{username}")
    public ResponseEntity<AdmiUsuario> getAdmiUsuarioByUsername(@PathVariable String username) {
        AdmiUsuario admiUsuario = admiUsuarioService.getAdmiUsuarioByUsername(username);
        if (admiUsuario != null) {
            return ResponseEntity.ok(admiUsuario);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/save")
    public ResponseEntity<AdmiUsuario> createAdmiUsuario(@RequestBody AdmiUsuarioRequest admiUsuario) {
        AdmiUsuario createdAdmiUsuario = admiUsuarioService.createAdmiUsuario(admiUsuario);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdAdmiUsuario);
    }

    @PutMapping("update/{id}")
    public ResponseEntity<AdmiUsuario> updateAdmiUsuario(@PathVariable Long id, @RequestBody AdmiUsuario admiUsuario) {
        AdmiUsuario updatedAdmiUsuario = admiUsuarioService.updateAdmiUsuario(id, admiUsuario);
        if (updatedAdmiUsuario != null) {
            return ResponseEntity.ok(updatedAdmiUsuario);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("delete/{id}")
    public ResponseEntity<Void> deleteAdmiUsuario(@PathVariable Long id) {
        boolean deleted = admiUsuarioService.deleteAdmiUsuario(id);
        if (deleted) {
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
    @PutMapping("/change-state/{id}")
    public ResponseEntity<AdmiUsuario> changeRoleState(@PathVariable Long id) {
        AdmiUsuario admiUsuario = admiUsuarioService.changeUserState(id);
        if (admiUsuario != null) {
            return ResponseEntity.ok(admiUsuario);
        } else {
            return ResponseEntity.notFound().build();
        }
    }
    @GetMapping("/roles/{id}")
    public ResponseEntity<List<AdmiRol>> getAllRolesByUserId(@PathVariable Long id) {
        List<AdmiRol> roles = admiUsuarioService.getAllRolesByUserId(id);
        return ResponseEntity.ok(roles);
    }
    @ExceptionHandler(ValidationException.class)
    public ResponseEntity<ErrorResponse> handleValidationException(ValidationException ex) {
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setErrorCode(HttpStatus.BAD_REQUEST.value());
        errorResponse.setErrorMessage(ex.getMessage());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
    }
}
