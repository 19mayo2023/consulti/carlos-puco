package ec.telconet.entity;

import java.util.Date;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.Data;

@Entity
@Data
@Table(name = "admi_usuario")
public class AdmiUsuario {
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequence_generator")
  @SequenceGenerator(name = "sequence_generator", sequenceName = "tu_secuencia", allocationSize = 1)
  @Column(name = "id")
  private Long id;

  @Column(name = "usuario")
  private String usuario;

  @Column(name = "password")
  private String password;

  @Column(name = "nombres")
  private String nombres;

  @Column(name = "apellidos")
  private String apellidos;

  @Column(name = "direccion")
  private String direccion;

  @Column(name = "telefono")
  private Long telefono;

  @Column(name = "usuario_creacion")
  private String usuarioCreacion;

  @Column(name = "fecha_creacion")
  private Date fechaCreacion;

  @Column(name = "foto")
  private String foto;
  @Column(name = "status")
  private String status;

  // Constructor, getters y setters
}
