package ec.telconet.service.implement;

import ec.telconet.entity.AdmiUsuario;
import ec.telconet.repository.AdmiUsuarioRepository;
import ec.telconet.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginServiceImpl implements LoginService {

    private final AdmiUsuarioRepository usuarioRepository;

    @Autowired
    public LoginServiceImpl(AdmiUsuarioRepository usuarioRepository) {
        this.usuarioRepository = usuarioRepository;
    }
              /*
     * funtion: Funcion para atenticarse
     * param: List<AdmiUsuario>
     * return:any
     * author: carlos.puco.mb@gamil.com
     * version:1.0
     */

    @Override
    public boolean authenticate(String username, String password) {
        AdmiUsuario usuario = usuarioRepository.findByUsuario(username);
        if (usuario != null) {
            // Realizar la validación del password sin utilizar PasswordEncoder
            return password.equals(usuario.getPassword());
        }
        return false;
    }
}
