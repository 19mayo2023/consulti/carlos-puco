package ec.telconet.service;

public interface LoginService {
    boolean authenticate(String username, String password);

}
