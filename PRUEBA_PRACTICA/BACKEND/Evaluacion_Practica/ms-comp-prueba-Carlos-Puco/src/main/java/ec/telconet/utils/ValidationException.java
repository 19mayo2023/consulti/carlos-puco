package ec.telconet.utils;

public class ValidationException extends RuntimeException {
    public ValidationException(String message) {
        super(message);
    }
}