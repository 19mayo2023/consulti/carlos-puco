package ec.telconet.controller;

import ec.telconet.entity.AdmiRol;
import ec.telconet.service.AdminRolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/admin-roles")
public class AdminRolController {
    private final AdminRolService adminRolService;

    @Autowired
    public AdminRolController(AdminRolService adminRolService) {
        this.adminRolService = adminRolService;
    }

    @GetMapping("all")
    public ResponseEntity<List<AdmiRol>> getAllAdminRoles() {
        List<AdmiRol> adminRoles = adminRolService.getAllAdminRoles();
        return ResponseEntity.ok(adminRoles);
    }

    @GetMapping("/{id}")
    public ResponseEntity<AdmiRol> getAdminRolById(@PathVariable Long id) {
        AdmiRol adminRol = adminRolService.getAdminRolById(id);
        if (adminRol != null) {
            return ResponseEntity.ok(adminRol);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/save")
    public ResponseEntity<AdmiRol> createAdminRol(@RequestBody AdmiRol adminRol) {
        AdmiRol createdAdminRol = adminRolService.createAdminRol(adminRol);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdAdminRol);
    }

    @PutMapping("update/{id}")
    public ResponseEntity<AdmiRol> updateAdminRol(@PathVariable Long id, @RequestBody AdmiRol adminRol) {
        AdmiRol updatedAdminRol = adminRolService.updateAdminRol(id, adminRol);
        if (updatedAdminRol != null) {
            return ResponseEntity.ok(updatedAdminRol);
        } else {
            return ResponseEntity.notFound().build();
        }
    }
    @PutMapping("/change-state/{id}")
    public ResponseEntity<AdmiRol> changeRoleState(@PathVariable Long id) {
        AdmiRol updatedAdminRol = adminRolService.changeRoleState(id);
        if (updatedAdminRol != null) {
            return ResponseEntity.ok(updatedAdminRol);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("delete/{id}")
    public ResponseEntity<Void> deleteAdminRol(@PathVariable Long id) {
        boolean deleted = adminRolService.deleteAdminRol(id);
        if (deleted) {
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
