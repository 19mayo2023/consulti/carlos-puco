package ec.telconet.utils;

import lombok.Data;

@Data
public class ErrorResponse {
    private int errorCode;
    private String errorMessage;

    // Constructor, getters y setters
}