package ec.telconet.export;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;

import ec.telconet.entity.*;

@Component

public class ExcelExporter {
    public void exportDataToExcel(List<AdmiRol> admiRoles, List<AdmiUsuario> admiUsuarios, List<InfoUsuarioRol> infoUsuarioRoles) {
        // Crear un nuevo libro de Excel
        Workbook workbook = new XSSFWorkbook();

        // Crear una nueva hoja de Excel
        Sheet sheet = workbook.createSheet("Datos");

        // Crear el encabezado de las columnas
        Row headerRow = sheet.createRow(0);
        headerRow.createCell(0).setCellValue("ID");
        headerRow.createCell(1).setCellValue("Nombre");
        headerRow.createCell(2).setCellValue("Descripción");

        // Escribir los datos de AdmiRol
        int rowNum = 1;
        for (AdmiRol admiRol : admiRoles) {
            Row row = sheet.createRow(rowNum++);
            row.createCell(0).setCellValue(admiRol.getId());
            row.createCell(1).setCellValue(admiRol.getNombre());
            row.createCell(2).setCellValue(admiRol.getDescripcion());
        }

        int rowNum2 = 1;
        for (AdmiUsuario admiUsuario : admiUsuarios) {
            Row row = sheet.createRow(rowNum2++);
            row.createCell(0).setCellValue(admiUsuario.getId());
            row.createCell(1).setCellValue(admiUsuario.getUsuario());
            row.createCell(2).setCellValue(admiUsuario.getPassword());
            row.createCell(3).setCellValue(admiUsuario.getNombres());
            row.createCell(4).setCellValue(admiUsuario.getApellidos());
            row.createCell(5).setCellValue(admiUsuario.getDireccion());
            row.createCell(6).setCellValue(admiUsuario.getTelefono());
            row.createCell(7).setCellValue(admiUsuario.getUsuarioCreacion());
            row.createCell(8).setCellValue(admiUsuario.getFechaCreacion());
            row.createCell(9).setCellValue(admiUsuario.getFoto());
            row.createCell(10).setCellValue(admiUsuario.getStatus());
        }
        // Escribir los datos de AdmiUsuario
        // ...
        

        int rowNum3 = 1;
        for (InfoUsuarioRol infoUsuarioRol : infoUsuarioRoles) {
            Row row = sheet.createRow(rowNum3++);
            row.createCell(0).setCellValue(infoUsuarioRol.getId());
            row.createCell(1).setCellValue(infoUsuarioRol.getRol().toString());
            row.createCell(2).setCellValue(infoUsuarioRol.getUsuarioCreacion());
            row.createCell(3).setCellValue(infoUsuarioRol.getFechaCreacion());
            row.createCell(4).setCellValue(infoUsuarioRol.getUsuarioModificacion());
            row.createCell(5).setCellValue(infoUsuarioRol.getFechaModificacion());
            row.createCell(2).setCellValue(infoUsuarioRol.getEstado());
        }
        // Escribir los datos de InfoUsuarioRol
        // ...

        // Guardar el archivo de Excel
        try (FileOutputStream fileOut = new FileOutputStream("datos.xlsx")) {
            workbook.write(fileOut);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
