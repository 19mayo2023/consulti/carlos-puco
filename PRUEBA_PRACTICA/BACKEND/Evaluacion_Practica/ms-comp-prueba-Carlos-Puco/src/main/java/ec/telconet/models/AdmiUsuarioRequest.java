package ec.telconet.models;


import java.util.List;
import ec.telconet.entity.AdmiRol;
import ec.telconet.entity.AdmiUsuario;
import lombok.Data;

@Data
public class AdmiUsuarioRequest {
   private AdmiUsuario admiUsuario;
    private List<AdmiRol> admiRolList;

}
