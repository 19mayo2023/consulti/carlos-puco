package ec.telconet;

import ec.telconet.service.InicializacionService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.*;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Import;

import ec.telconet.config.CorsConfig;

@SpringBootApplication
@Import(CorsConfig.class)
public class EvaluacionPracticaApplication {

	public static void main(String[] args) throws Exception {
		ConfigurableApplicationContext context = SpringApplication.run(EvaluacionPracticaApplication.class, args);
		InicializacionService inicializacionService = context.getBean(InicializacionService.class);
		inicializacionService.inicializar();
	}

}
