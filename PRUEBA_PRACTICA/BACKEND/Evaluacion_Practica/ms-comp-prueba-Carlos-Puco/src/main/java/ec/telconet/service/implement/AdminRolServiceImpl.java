package ec.telconet.service.implement;

import ec.telconet.entity.AdmiRol;
import ec.telconet.entity.InfoUsuarioRol;
import ec.telconet.repository.AdmiRolRepository;
import ec.telconet.repository.InfoUsuarioRolRepository;
import ec.telconet.service.AdminRolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdminRolServiceImpl implements AdminRolService {

    private final AdmiRolRepository adminRolRepository;
    private final InfoUsuarioRolRepository infoUsuarioRolRepository;

    @Autowired
    public AdminRolServiceImpl(AdmiRolRepository adminRolRepository, InfoUsuarioRolRepository infoUsuarioRolRepository) {
        this.adminRolRepository = adminRolRepository;
        this.infoUsuarioRolRepository = infoUsuarioRolRepository;
    }

    /*
     * funtion: Funcion para traer todos lo roles
     * param: List<AdmiRol>
     * return:any
     * author: carlos.puco.mb@gamil.com
     * version:1.0
     */

    @Override
    public List<AdmiRol> getAllAdminRoles() {
        return adminRolRepository.findAll();
    }

       /*
     * funtion: Funcion para tarer el ROL de adminitrador
     * param: AdmiRol
     * return:Long id
     * author: carlos.puco.mb@gamil.com
     * version:1.0
     */
    @Override
    public AdmiRol getAdminRolById(Long id) {
        return adminRolRepository.findById(id).orElse(null);
    }

          /*
     * funtion: Funcion para tarer el ROL de adminitrador
     * param: AdmiRol
     * return:Long id
     * author: carlos.puco.mb@gamil.com
     * version:1.0
     */

    @Override
    public AdmiRol createAdminRol(AdmiRol adminRol) {
        return adminRolRepository.save(adminRol);
    }
           /*
     * funtion: Funcion para editar el tipo de rol
     * param: AdmiRol
     * return:Long id, AdmiRol adminRol
     * author: carlos.puco.mb@gamil.com
     * version:1.0
     */


    @Override
    public AdmiRol updateAdminRol(Long id, AdmiRol adminRol) {
        AdmiRol existingAdminRol = adminRolRepository.findById(id).orElse(null);

        if (existingAdminRol != null) {
            existingAdminRol.setNombre(adminRol.getNombre());
            existingAdminRol.setDescripcion(adminRol.getDescripcion());
            // Actualizar cualquier otro atributo según corresponda
            return adminRolRepository.save(existingAdminRol);
        }
        return null;
    }

          /*
     * funtion: Funcion para cambiar el status de Rol 
     * param: AdmiRol
     * return:Long id
     * author: carlos.puco.mb@gamil.com
     * version:1.0
     */


    @Override
    public AdmiRol changeRoleState(Long id) {
        AdmiRol adminRol = adminRolRepository.findById(id).orElse(null);
        if (adminRol != null) {
            adminRol.setStatus(adminRol.getStatus().equals("Activo") ? "Inactivo" : "Activo");
            return adminRolRepository.save(adminRol);
        }
        return null;
    }

    
          /*
     * funtion: Funcion para eliminar un ROL
     * param: boolean
     * return:Long id
     * author: carlos.puco.mb@gamil.com
     * version:1.0
     */


    @Override
    public boolean deleteAdminRol(Long id) {
        // Verificar si el rol existe
        if (!adminRolRepository.existsById(id)) {
            return false;
        }
        // Desasociar el rol de los usuarios
        desasociarRolDeUsuarios(id);

        // Eliminar el rol
        adminRolRepository.deleteById(id);

        return true;
    }

    
          /*
     * funtion: Funcion para que al momento de borara el rol el usuario ya no tenga la asiciacion
     * param: (Long rolId
     * return:any
     * author: carlos.puco.mb@gamil.com
     * version:1.0
     */


    public void desasociarRolDeUsuarios(Long rolId) {
        // Buscar los registros de InfoUsuarioRol asociados al rol
        List<InfoUsuarioRol> infoUsuarioRols = infoUsuarioRolRepository.findByRolId(rolId);

        // Desasociar el rol de los usuarios
        for (InfoUsuarioRol infoUsuarioRol : infoUsuarioRols) {
            infoUsuarioRol.setRol(null);
            infoUsuarioRolRepository.save(infoUsuarioRol);
        }
    }
}
