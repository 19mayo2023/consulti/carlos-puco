package ec.telconet.repository;

import java.util.List;
import ec.telconet.entity.AdmiRol;
import ec.telconet.entity.AdmiUsuario;
import jakarta.persistence.Tuple;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AdmiUsuarioRepository extends JpaRepository<AdmiUsuario, Long> {
    // Métodos personalizados del repositorio, si los necesitas
    AdmiUsuario findByUsuario(String usuario);
    List<Tuple> findAllById(@Param("id") Long id);
}
