package ec.telconet.repository;

import ec.telconet.entity.AdmiRol;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdmiRolRepository extends JpaRepository<AdmiRol, Long> {
    // Métodos personalizados del repositorio, si los necesitas
    AdmiRol findByNombre(String nombre);
}
