package ec.telconet.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.telconet.entity.AdmiRol;
import ec.telconet.entity.AdmiUsuario;
import ec.telconet.entity.InfoUsuarioRol;
import ec.telconet.export.ExcelExporter;
import ec.telconet.repository.AdmiRolRepository;
import ec.telconet.repository.AdmiUsuarioRepository;
import ec.telconet.repository.InfoUsuarioRolRepository;

@RestController
@RequestMapping("/export")
public class ExcelController {

    @Autowired
    private ExcelExporter excelExporter;

    @Autowired
    private AdmiRolRepository admiRolRepository;

    @Autowired
    private AdmiUsuarioRepository admiUsuarioRepository;

    @Autowired
    private InfoUsuarioRolRepository infoUsuarioRolRepository;

    @GetMapping("/excel")
    public ResponseEntity<byte[]> exportToExcel() {
        // Obtener los datos que se exportarán a Excel (pueden provenir de una base de datos u otra fuente)
        List<AdmiRol> admiRoles = admiRolRepository.findAll();
        List<AdmiUsuario> admiUsuarios = admiUsuarioRepository.findAll();
        List<InfoUsuarioRol> infoUsuarioRoles = infoUsuarioRolRepository.findAll();

        // Generar el archivo de Excel
        excelExporter.exportDataToExcel(admiRoles, admiUsuarios, infoUsuarioRoles);

        // Leer el archivo generado
        File excelFile = new File("datos.xlsx");
        byte[] excelBytes;
        try {
            excelBytes = Files.readAllBytes(excelFile.toPath());
        } catch (IOException e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }

        // Configurar la respuesta con el archivo de Excel
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        headers.setContentDispositionFormData("attachment", "datos.xlsx");
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");

        return new ResponseEntity<>(excelBytes, headers, HttpStatus.OK);
    }
}
