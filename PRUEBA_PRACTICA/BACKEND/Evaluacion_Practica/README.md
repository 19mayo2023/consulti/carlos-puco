# Git Repository

- Abrir la consola para clonar el Repositorio
- Clonar el repositorio con el comando 
```
git clone https://gitlab.com/19mayo2023/consulti/carlos-puco.git
```
- Abrir la Carpera `PRUEBA_PRACTICA/BACKEND`

# Running the application locally-dev
- Ejecute vs Code.
- Importar el proyecto.(ARCHIVO-> Abrir proyectos desde el sistema de archivos ->Directoty)
- La palicacion debe estar en la direccion dnde clonaste el repositorio
- Una vez ubicados en el carpeta del repostorio nos vamos `PRUEBA_PRACTICA/BACKEND`
- Buscar el proyecto y pulsar sobre Finalizar.
- Crear la conexión a la base de datos
- Ejecutar pgADMIN
- Crear una base de datos con el nombre: prueba
- Ejecutar el script SQL de la base de datos en la consola, se encuentra en la carpeta: `SCRIPTS`
- crear una nueva conexión entre el proyecto y la base de datos
- Click en `run` en el archivo EvaluacionPracticaApplication.java
- La aplicacion se levanta en el http://localhost:5000
    - Si se desea probar el importar el archivo postam que esta en la carpeta `DOC`


# Running the application docker

- Iniciar docker desktop **Important**
- Open the terminal and go to the root directory where docker-compose.yml is located and run the following command.
- Asegurarse que el archivo Dockerfile este en la raiz del proyecto
- Asegurarse que el archiov docker.compose.yml este en la raiz del proyecto 
- Colocar el siguiente archivo en la carpeta target del proyecto, link en el siguiente enlace
    ```fetch
    https://drive.google.com/drive/folders/1noiltcZNK6aWiMq2-12TmGfbc8Bmqtb0?usp=sharing
    ```
- En la raiz del proyecto correr para levantar el proyecto:
    - `docker-compose up`
    - El poryecto se levantara en el puerto 5000





