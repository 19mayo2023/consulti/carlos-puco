# Requirements

**Docker-Desktop** [enter link description here](https://www.docker.com/products/docker-desktop/)
**Git**  [enter link description here](https://git-scm.com/downloads)
**NodeJS 16**  [enter link description here](https://nodejs.org/en/blog/release/v16.16.0)

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.2.1.

# Git Repository
Run in terminal CMD to  download the project.
```
git clone https://gitlab.com/19mayo2023/consulti/carlos-puco.git
```

# Running the application locally-dev

Una vez clonado el repostorio abri la capeta `PRUEBA_PRACTICA/FRONTEND`
 - Correr ene la consola los siguientes comando
 - `npm i`
 - `ng serve`

# Running the application docker

 1. Inciar docker deskopt **Important**
 2. Abrir el terminal en la raiz de poyecto.
- En la raiz del proyecto corremos el comando 
```shell
ng build
``` 
- Despues corremos el comando 
```shell
docker build -t app-front-prueba-carlos-puco .
```
- Comprobar imagen 
```shell
docker images
```
- Continuamos con el comando para exponer el contendor en los puertos se los puede cambiar 
```shell
docker run -d -it -p 8000:80 app-front-prueba-carlos-puco
```
El proyecto se dockeriza en el puerto 800a





