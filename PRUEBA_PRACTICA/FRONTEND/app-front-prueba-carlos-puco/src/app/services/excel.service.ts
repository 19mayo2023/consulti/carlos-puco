import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


const URL = "http://localhost:5000"
const ROL = URL + '/export/excel';
@Injectable({
  providedIn: 'root'
})
export class ExcelService {

  constructor(private http: HttpClient) { }

  exportToExcel(): void {
    this.http.get(ROL, { responseType: 'blob' })
      .subscribe((data: Blob) => {
        const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' });
        const url = window.URL.createObjectURL(blob);
        const link = document.createElement('a');
        link.href = url;
        link.download = 'datos.xlsx';
        link.click();
        window.URL.revokeObjectURL(url);
      });
  }
}
