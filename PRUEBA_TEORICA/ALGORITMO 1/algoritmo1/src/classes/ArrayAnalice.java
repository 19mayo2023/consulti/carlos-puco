package classes;

public class ArrayAnalice {
    /*
     * funcion: Validar si esta dentro del rango
     * param: int
     * return: boolean
     * author:carlos.puco.mb@gmail.com
     * version:1.0
     */

    public boolean isValidElement(int element) {
        return element >= 1 && element <= 9;
    }
    /*
     * funcion: Cuenta el numero de veces que se repite un numero
     * param:int[] array
     * return: int
     * author:carlos.puco.mb@gmail.com
     * version:1.0
     */

    public int getMaxConsecutiveCount(int[] array) {
        int currentCount = 1;
        int maxCount = 1;

        for (int i = 1; i < array.length; i++) {
            if (array[i] == array[i - 1]) {
                currentCount++;
                if (currentCount > maxCount) {
                    maxCount = currentCount;
                }
            } else {
                currentCount = 1;
            }
        }

        return maxCount;
    }

      /*
     * funcion: Recupera el nuemro que mas veces se repitio
     * param: int[] array, int maxCount
     * return: int
     * author:carlos.puco.mb@gmail.com
     * version:1.0
     */

    public int getMostFrequentNumber(int[] array, int maxCount) {
        int mostFrequentNumber = array[0];
        int currentCount = 1;

        for (int i = 1; i < array.length; i++) {
            if (array[i] == array[i - 1]) {
                currentCount++;
                if (currentCount == maxCount) {
                    mostFrequentNumber = array[i];
                    break;
                }
            } else {
                currentCount = 1;
            }
        }

        return mostFrequentNumber;
    }
}
