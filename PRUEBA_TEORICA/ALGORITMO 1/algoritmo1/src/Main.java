import classes.ArrayAnalice;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Ingrese el tamaño que desea para el arreglo: ");
        int size = scanner.nextInt();
        int[] myArray = new int[size];

        ArrayAnalice validator = new ArrayAnalice();

        System.out.println("Recuerde que el arreglo solo resive nuemro del 1-9 ");
        for (int i = 0; i < size; i++) {
            int element;
            do {
                System.out.print("Elemento " + (i + 1) + ": ");
                element = scanner.nextInt();
                if (element<1 || element>9){
                    System.out.print("Elemento no valido ingreselo de nuevo \n");
                }
            } while (!validator.isValidElement(element));
            myArray[i] = element;
        }

        ArrayAnalice arrayAnalyzer = new ArrayAnalice();
        int maxCount = arrayAnalyzer.getMaxConsecutiveCount(myArray);
        int mostFrequentNumber = arrayAnalyzer.getMostFrequentNumber(myArray, maxCount);

        System.out.println("Recurrencias: " + maxCount);
        System.out.println("Número: " + mostFrequentNumber);
    }
    }
