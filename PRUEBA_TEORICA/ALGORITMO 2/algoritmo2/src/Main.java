import classes.Position;
import classes.Print;

public class Main {
    public static void main(String[] args) {
        int[] myArray = {1,2,-1,1,0,1,2,-1,-1,-2};

        Position positionAnalyzer = new Position();
        char[][] area = positionAnalyzer.calculateFinalArea(myArray);

        Print areaPrinter = new Print();
        System.out.println("----Position Tab----");
        areaPrinter.printArea(area);
    }
}