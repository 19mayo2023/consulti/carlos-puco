package classes;

/*
 * funcion: Binuja en que posisicion se quedo la ficha
 * param: array[]
 * return: void
 * author:carlos.puco.mb@gmail.com
 * version:1.0
 */

public class Print {
    public void printArea(char[][] area) {
        for (char[] row : area) {
            System.out.println(new String(row));
        }
    }
}
