package classes;

public class Position {

    /*
     * funcion: Calcular movieitnos de la fiche
     * param: array[]
     * return: char [][]
     * author:carlos.puco.mb@gmail.com
     * version:1.0
     */

    public char[][] calculateFinalArea(int[] myArray) {
        char[][] area = new char[4][4]; // Área de 4x4

        int x = 0;
        int y = 0;

        // Realizar los movimientos
        for (int i = 0; i < myArray.length; i += 2) {
            int dx = myArray[i]; // Movimiento en el eje horizontal
            int dy = myArray[i + 1]; // Movimiento en el eje vertical

            // Actualizar la posición de la X
            x += dx;
            y += dy;

            // Asegurarse de que la X no salga del área de 4x4
            x = Math.max(0, Math.min(x, 3));
            y = Math.max(0, Math.min(y, 3));
        }

        // Llenar el área con "O"
        for (int i = 0; i < area.length; i++) {
            for (int j = 0; j < area[i].length; j++) {
                area[i][j] = 'O';
            }
        }

        // Colocar la X en su posición final
        area[y][x] = 'X';

        return area;
    }
}
